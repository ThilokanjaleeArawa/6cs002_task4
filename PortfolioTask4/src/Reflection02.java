public class Reflection02 {
		  public static void main(String[] args) {
			  
		    ReflectionSimple simple = new ReflectionSimple();
		    
		    simple.squareX();
		    // s.squareX(); // if you uncomment this you will get a compiler error
		    
		    double x = simple.x;
		    // double y = simple.y; // if you uncomment this you will get a compiler error
		    
		    System.out.println("Reflection02\n" + simple);
		  }
		}

