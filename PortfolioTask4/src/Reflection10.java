import java.lang.reflect.Method;

public class Reflection10 {
  public static void main(String[] args) throws Exception {
    ReflectionSimple simple = new ReflectionSimple();
    System.out.println("Reflection10");
    Method m = simple.getClass().getDeclaredMethod("setX", double.class);
    m.setAccessible(true);
    m.invoke(simple, 53.4);
    System.out.println(simple);
  }
}
