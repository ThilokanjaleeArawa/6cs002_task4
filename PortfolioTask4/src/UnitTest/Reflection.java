package UnitTest;

public class Reflection {
	
	public double x = 35.2;
	private double y = 83.6;
	
	public Reflection() {	
	}
	public Reflection(double x, double y) {
		this.x = x;
		this.y = y;
	}
	public void squareX() {
		this.x = Math.sqrt(this.x);
	}	
	private void squareY() {
		this.y = Math.sqrt(this.y);
	}	
	public double getX() {
		return x;
	}	
	private void setX(double x) {
		this.x = x;
	}	
	public double getY() {
		return y;
	}	
	public void setY(double y) {
		this.y = y;
	}	
	public String toString() {
		return String.format("\nx : %.2f\ny : %.2f " , x, y);
	}
}
