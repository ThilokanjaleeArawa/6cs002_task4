package UnitTest;
import static KUnitTest.KUnit.*;

public class ReflectionTest {

  void Reflectiontest1(){
	  
	  Reflection reflection = new Reflection(35.2, 83.6);
	  test1(reflection.getX(), 35.2);
	  test1(reflection.getY(), 83.6);
	  test2(reflection.getY(), 83.6);    
	  test2(reflection.getY(), 35.2);    
  }

	void Reflectiontest2(){
		
		Reflection reflection = new Reflection(35.2, 83.6);
		reflection.squareX();
	    test1(reflection.getX(), 35.2);
	}
	

void Reflectiontest3(){
		Reflection reflection = new Reflection(35.2, 83.6);
		  test4("2362489", "8521");
		  reflection = new Reflection(35.2, 0);
		  test4("Thilokanjalee", "Arawa");
		  reflection = new Reflection(83.6, 0);
		  test4("T", "A");
		  
	}

  public static void main(String[] args) {
	  
	  ReflectionTest test = new ReflectionTest();
	  test.Reflectiontest1();
	  test.Reflectiontest2();
	  test.Reflectiontest3();
      report();
  }
}
