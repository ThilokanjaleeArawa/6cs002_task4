import java.lang.reflect.Field;

public class Reflection04 {
  public static void main(String[] args) throws Exception {
	  
    ReflectionSimple simple = new ReflectionSimple();
    System.out.println("Reflection04");
    Field[] fields = simple.getClass().getFields();
    
    System.out.printf("There is %d field\n", fields.length);
    
    for (Field f : fields) {
    	System.out.printf("field name = %s \ttype = %s  \tvalue = %.2f\n", f.getName(), 
    	          f.getType(), f.getDouble(simple));
    }
  }
}
