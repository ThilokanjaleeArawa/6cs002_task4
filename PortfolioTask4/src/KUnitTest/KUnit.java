package KUnitTest;

import java.util.*;

public class KUnit {
  private static List<String> test;
  private static int ChecksMade = 0;
  private static int PassedChecks = 0;
  private static int FailedChecks = 0;
  
  private static void addToReport(String txt) {  
    if (test == null) {
    	
    	test = new LinkedList<String>();
    }
    test.add(String.format("Test case %d: %s", ChecksMade++, txt));
  }

  public static void test1(double x, double y) {
	    if (x == y) {
	    	addToReport(String.format("  %.2f  ==  %.2f", x, y));
	    	PassedChecks++;	    	
	    } else {
	    	addToReport(String.format("*  %.2f  ==  %.2f", x, y));
	    	FailedChecks++;
	    }
	  }

  public static void test2(double x, double y) {
	    if (x != y) {
	    	addToReport(String.format("  %.2f  !=  %.2f", x, y));
	    	PassedChecks++;	    	
	    } else {
	    	addToReport(String.format("* %.2f  !=  %.2f", x, y));
	    	FailedChecks++;
	    	
	    }} 
  public static void test3(String value1, String value2) {
	  try {
	        char n1 = value1.charAt(5);
	        char n2 = value2.charAt(5);
	        if (value1.length() >= 5 && value2.length() >= 5) {
	            if (Character.isDigit(n1) || Character.isDigit(n2)) {
	            	addToReport(String.format("%s and %s consist Letters and Numbers", value1, value2));
	            	ChecksMade++;
	            } else if (value1.charAt(5) == n1 && value2.charAt(5) == n2) {
	            	addToReport(String.format("%s and %s consist Expected Characters", value1, value2));
	            	PassedChecks++;
	            } else {
	            	addToReport(String.format("%s and %s does not consist Expected Characters", value1, value2));
	            	FailedChecks++;
	            }
	        } 
	    } catch (StringIndexOutOfBoundsException e) {
	        System.out.println("Both of the Strings should be greater than 5 Letters");
	    }
	}
  public static void test4(String x, String y) {
		try {			
			char val1 = x.charAt(4);
			char val2 = y.charAt(4);
			if (x.charAt(4) == val1 && y.charAt(4) == val2) {
				addToReport(String.format("%s and %s Total no of Strings are greater than 4.", x, y));
				PassedChecks++;
		    } else {
		    	addToReport(String.format("%s and %s greater than 4 no of Strings.", x, y));
		    	FailedChecks++;
		    }			
		}catch(StringIndexOutOfBoundsException e) {
			addToReport(String.format("Total no of Strings should be greater than 4."));
			FailedChecks++;
		}		
	  }
	  
  public static void report() { 
    System.out.printf("Number of Tests Passed : %d\n", PassedChecks);
    System.out.printf("Number of Tests Failed : %d\n", FailedChecks);
    System.out.println();
    
    for (String t : test) {
      System.out.println(t);
      
    }
  }
}
 
