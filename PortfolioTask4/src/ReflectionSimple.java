public class ReflectionSimple {
	public double x = 14.2;
	private double y = 20.8;

	public ReflectionSimple() {
	}

	public ReflectionSimple(double x, double y) {
		this.x = x;
	    this.y = y;
	}

	public void squareX() {
		this.x = Math.sqrt(this.x);
	}

	private void squareY() {
		this.y = Math.sqrt(this.y);
	}

	public double getX() {
		return x;
	}

	public double getY() {
	    return y;
	}
	  
	private void setX(double x) {
		this.x *= this.x;
	}
	  
	private void setY(double b) {
		this.y *= this.y;
	}

	public String toString() {
		return String.format("x : %.2f\ny : %.2f", x, y);
	}
			    
}
	

