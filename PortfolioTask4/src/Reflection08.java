import java.lang.reflect.Field;

public class Reflection08 {
	
	public static void main(String[] args) throws Exception {
		
		ReflectionSimple simple = new ReflectionSimple();
	    System.out.println("Reflection08");
	    
	    Field[] fields = simple.getClass().getDeclaredFields();
	    
	    System.out.printf("There are %d fields\n", fields.length);
	    
	    for (Field f : fields) {
	    	
	      f.setAccessible(true);
	      Double num = f.getDouble(simple);
	      num++;
	      f.setDouble(simple, num);
	      System.out.printf("field name = %s \ttype = %s  \tvalue = %.2f\n", f.getName(), 
	          f.getType(), f.getDouble(simple));
	    }
	  }

}
